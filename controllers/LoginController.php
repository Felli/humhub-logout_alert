<?php
/**
 * Logout Alert
 * @link https://gitlab.com/funkycram/humhub-logout_alert
 * @license https://gitlab.com/funkycram/humhub-logout_alert/blob/master/LICENSE
 * @author [FunkycraM](https://marc.fun)
 */

namespace humhub\modules\logout_alert\controllers;

use Yii;

class LoginController extends \humhub\components\Controller
{
    public function getAccessRules()
    {
        return [
             ['login']
        ];
    }
        
    public function init()
    {
        return parent::init();
    }

    public function actionIndex()
    {
    	return $this->renderPartial('index', [
        ]);
    }
}