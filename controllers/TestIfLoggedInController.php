<?php
/**
 * Logout Alert
 * @link https://gitlab.com/funkycram/humhub-logout_alert
 * @license https://gitlab.com/funkycram/humhub-logout_alert/blob/master/LICENSE
 * @author [FunkycraM](https://marc.fun)
 */

namespace humhub\modules\logout_alert\controllers;

use Yii;

class TestIfLoggedInController extends \humhub\components\Controller
{
    public function actionIndex()
    {
    	if (!Yii::$app->user->isGuest)
    		return true;
    }
}