Module Logout Alert
=================

## Description

Show a modal box if the computer has just waking from sleep mode (standby or hibernation) and user is disconnected.

Prevent users from loosing what they were writing as they might be disconnected during computer sleep.

__Link:__
https://gitlab.com/funkycram/humhub-logout_alert

__License:__
https://gitlab.com/funkycram/humhub-logout_alert/blob/master/LICENSE

__Author:__       
[FunkycraM](https://marc.fun)