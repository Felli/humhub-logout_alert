<?php
return [
	'You have been logged out' => 'Vous avez été déconnecté',
	'A new window will be opened to reconnect to the platform. Once connected, you can close the window and continue using the platform.' => 'Une nouvelle fenêtre va s‘ouvrir pour vous reconnecter à la plateforme. Une fois connecté, vous pourrez fermer la fenêtre et continuer à utiliser la plateforme.',
	'Reconnect to the plateform' => 'Se reconnecter à la plateforme',
	'You are now logged in again!' => 'Vous êtes à nouveau connecté !',
	'You can close this window to return to the plateform' => 'Vous pouvez fermer cette fenêtre pour revenir sur la plateforme',
];
?>