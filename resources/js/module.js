var checkFrequency = 2000;
var oldTime = 3000000000000;

// Test periodicaly of waken
setInterval(function() {
	var newTime = (new Date()).getTime();
	
	// If return from sleep mode
	if (newTime > (oldTime + checkFrequency + 2000)) {
		// Check if disconnected
		$.ajax({
			url: testIfLoggedInUrl,
			success:function(data) {
				if (!data) {
					showModalAlert();
				}
			},
			statusCode: {
			    404: function() {
			    	showModalAlert();
			    }
			}
		});
	}

	oldTime = newTime;
}, checkFrequency);


function showModalAlert() {
    $('#logout-alert-modal').modal({
        backdrop: 'static',
        keyboard: false,
        show: true
    });
};