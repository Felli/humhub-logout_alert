<?php
use yii\helpers\Html;
use yii\helpers\Url;

$bundle = \humhub\modules\logout_alert\assets\Assets::register($this);
$this->registerJsVar('testIfLoggedInUrl', Url::to(['/logout_alert/test-if-logged-in/index']));
?>

<div class="modal logout-alert-modal" id="logout-alert-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><?= Yii::t('LogoutAlertModule.base', 'You have been logged out') ?></h4>
            </div>
            <div class="modal-body">
                <?= Yii::t('LogoutAlertModule.base', 'A new window will be opened to reconnect to the platform. Once connected, you can close the window and continue using the platform.') ?>
            </div>
            <div class="modal-footer">
                <?= Html::a(
	                '<i class="fa fa-sign-in"></i> '.Yii::t('LogoutAlertModule.base', 'Reconnect to the plateform'),
	                Url::to(['/logout_alert/login/index']),
	                [
	                	'id' => 'relogin-button',
	                    'class' => 'btn-sxm btn btn-default',
	                    'target' => '_blank',
	                    'data-pjax' => '0',
	                ]
	            ) ?>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
	<?php // Remove duplicate after ajax reload ?>
	var i=0;
	$('.logout-alert-modal').each(function(){
		if (i > 0)
			$(this).detach();
		i++;
	});
	$('#relogin-button').on('click', function() {
		setTimeout(function() {
			$('#logout-alert-modal').modal('hide');
		}, 2000);
	});
</script>