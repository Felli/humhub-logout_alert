<?php
/**
 * Logout Alert
 * @link https://gitlab.com/funkycram/humhub-logout_alert
 * @license https://gitlab.com/funkycram/humhub-logout_alert/blob/master/LICENSE
 * @author [FunkycraM](https://marc.fun)
 */

namespace humhub\modules\logout_alert;

use yii\helpers\Url;

class Module extends \humhub\components\Module
{
    
    public function disable()
    {
        return parent::disable();
        // what needs to be done if module is completely disabled?
    }

    public function enable()
    {
        return parent::enable();
        // what needs to be done if module is enabled?
    }

    /**
     * @inheritdoc
     */
    public function getConfigUrl()
    {
        return Url::to([
            '/wake_alert/config'
        ]);
    }
}
