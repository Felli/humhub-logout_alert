<div class="logout-alert-module-container">
    <div class="panel panel-default">
    
        <div class="panel-heading">
            <h1>
                <strong><?= Yii::t('LogoutAlertModule.base', 'You are now logged in again!') ?></strong>
            </h1>
        </div>
    
        <div class="panel-body">
            <?= Yii::t('LogoutAlertModule.base', 'You can close this window to return to the plateform') ?>
        </div>
    
    </div>
</div>