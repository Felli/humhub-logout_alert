<?php
/**
 * Logout Alert
 * @link https://gitlab.com/funkycram/humhub-logout_alert
 * @license https://gitlab.com/funkycram/humhub-logout_alert/blob/master/LICENSE
 * @author [FunkycraM](https://marc.fun)
 */

use humhub\widgets\LayoutAddons;

return [
    'id' => 'logout_alert',
    'class' => 'humhub\modules\logout_alert\Module',
    'namespace' => 'humhub\modules\logout_alert',
    'events' => [
        [
            'class' => LayoutAddons::class,
            'event' => LayoutAddons::EVENT_INIT,
            'callback' => [
                'humhub\modules\logout_alert\Events',
                'onLayoutAddonsInit'
            ]
        ]
    ]
];