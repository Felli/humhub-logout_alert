<?php
/**
 * Logout Alert
 * @link https://gitlab.com/funkycram/humhub-logout_alert
 * @license https://gitlab.com/funkycram/humhub-logout_alert/blob/master/LICENSE
 * @author [FunkycraM](https://marc.fun)
 */

namespace humhub\modules\logout_alert\assets;

use yii\web\AssetBundle;

class Assets extends AssetBundle
{
    public $publishOptions = [
        'forceCopy' => false
    ];
    
    public $sourcePath = '@logout_alert/resources';

    public $css = [
    ];
    public $js = [
        'js/module.min.js',
    ];
}